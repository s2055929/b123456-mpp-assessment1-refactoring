import matplotlib.pyplot as plt
import re
import numpy as np


pattern_process_num = re.compile(r'(?<=percolate: running on )\d+')
pattern_L = re.compile(r'(?<=percolate: L = )\d+')
pattern_total_time = re.compile(r'(?<=total used time: )\d+\.?\d*')
pattern_para_time = re.compile(r'(?<=parallel part used time: )\d+\.?\d*')


f = open('/home/gao/study/coursework/MPP/percolate/test/parallel_run_result')
all_str = f.read()

process_num_list = pattern_process_num.findall(all_str)
L_list = pattern_L.findall(all_str)
total_time_list = pattern_total_time.findall(all_str)
para_time_list = pattern_para_time.findall(all_str)

for i in range(len(total_time_list)):
    total_time_list[i] = float(total_time_list[i])
    para_time_list[i] = float(para_time_list[i])

# x -> process num y -> time
# diff line -> L


plt.figure()
plt.plot(process_num_list[0:6], total_time_list[0:6], color="blue", linewidth=1, linestyle="-", label="L480totaltime")
plt.plot(process_num_list[0:6], total_time_list[6:12], color="red", linewidth=1, linestyle="-", label="L960totaltime")
plt.plot(process_num_list[0:6], total_time_list[12:18], color="green", linewidth=1, linestyle="-", label="L1440totaltime")
plt.plot(process_num_list[0:6], total_time_list[18:24], color="yellow", linewidth=1, linestyle="-", label="L1920totaltime")
plt.plot(process_num_list[0:6], para_time_list[0:6], color="blue", linewidth=1, linestyle="--", label="L480paratime")
plt.plot(process_num_list[0:6], para_time_list[6:12], color="red", linewidth=1, linestyle="--", label="L960paratime")
plt.plot(process_num_list[0:6], para_time_list[12:18], color="green", linewidth=1, linestyle="--", label="L1440paratime")
plt.plot(process_num_list[0:6], para_time_list[18:24], color="yellow", linewidth=1, linestyle="--", label="L1920paratime")
plt.title("")
plt.xticks(process_num_list[0:6])
plt.yticks([0, 0.5, 1.0, 1.5, 2.0, 5.0, 10.0, 15.0])
plt.xlabel("process number")
plt.ylabel("time cost")
plt.legend(loc = 'upper right')
plt.savefig('./graph.jpg')






print(1)











